package com.aliware.edas;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wanglan
 */
@RestController
@RefreshScope
public class EchoController {

	@Value("${test.name}")
	private String name;

	@RequestMapping(value = "/")
	public String echo() {
		return "获取的配置test.name："+name;
	}
}
