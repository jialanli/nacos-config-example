package com.aliware.edas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wanglan
 */
@SpringBootApplication
public class NacosConfigExampleApplication {
	public static void main(String[] args) {
		SpringApplication.run(NacosConfigExampleApplication.class, args);
	}
}
